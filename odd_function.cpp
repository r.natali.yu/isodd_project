#include <iostream>

void FindOddNumbers(int maxNumber, bool IsOdd)
{
    for(int i = int(IsOdd); i <= maxNumber; i += 2)
    {
        std::cout << i << ' ';
    }
}

int main()
{
    int N = 10;
    for (int i = 0; i <= N; i += 2)
    {
        std::cout << i << ' ';
    }
    std::cout << std::endl;

    FindOddNumbers(8, true);
}